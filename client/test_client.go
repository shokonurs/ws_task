package main

import (
	"bufio"
	"fmt"
	"github.com/gorilla/websocket"
	"os"
)

func main() {

	port := 8080
	serverAddr := fmt.Sprintf("ws://localhost:%v", port)
	c, _, err := websocket.DefaultDialer.Dial(serverAddr, nil)
	if err != nil {
		fmt.Println("Cant dial server", err)
		return
	}
	defer c.Close()

	msgType, body, err := c.ReadMessage()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(body))

	reader := bufio.NewReader(os.Stdin)
	var text string
	for {
		text, _ = reader.ReadString('\n')
		err = c.WriteMessage(msgType, []byte(text))
		if err != nil {
			fmt.Println(err)
			continue
		}

		_, value, err := c.ReadMessage()
		if err != nil {
			fmt.Println(err)
			continue
		}

		fmt.Println(string(value))
	}
}
