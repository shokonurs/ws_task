package server

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"task/internal/services"
)

type Server struct {
	rng      services.UniqueRandomIntGenerator
	upgrader websocket.Upgrader
}

func New(rng services.UniqueRandomIntGenerator) *Server {

	return &Server{
		rng: rng,
		upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool { return true },
		},
	}
}

func (s *Server) Run(portAddress int) {

	r := mux.NewRouter()
	r.HandleFunc("/", s.serveClient)
	err := http.ListenAndServe(fmt.Sprintf(":%v", portAddress), r)
	if err != nil {
		log.Printf("can't start server:%v", err)
	}
}

func (s *Server) serveClient(w http.ResponseWriter, r *http.Request) {

	ws, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		err = json.NewEncoder(w).Encode("unable to create websocket connection.please try again later")
		if err != nil {
			log.Printf("serveClient.upgrader: %v", err)
		}
		return
	}
	defer ws.Close()

	err = ws.WriteMessage(1, []byte("Hello! Please send me any message to receive a random big Int"))
	if err != nil {
		log.Printf("serveClient.WriteMessage: %v", err)
		return
	}

	s.sendRandomNumber(ws)
}

func (s *Server) sendRandomNumber(ws *websocket.Conn) {

	for {
		msgType, _, err := ws.ReadMessage()

		n, err := s.rng.GetUniqueRandomBigInt()
		if err != nil {
			log.Printf("sendRandomNumber.GetUniqueRandomBigInt: %v", err)
			err = ws.WriteMessage(msgType, []byte("unable to get random number.please try again later"))
			if err != nil {
				log.Printf("sendRandomNumber.GetUniqueRandomBigInt: %v", err)
			}
			continue
		}

		err = ws.WriteMessage(msgType, []byte(n.String()))
		if err != nil {
			log.Printf("sendRandomNumber.GetUniqueRandomBigInt: %v", err)
		}
	}

}
