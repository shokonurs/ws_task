package services

import "math/big"

type UniqueRandomIntGenerator interface {
	GetUniqueRandomBigInt() (*big.Int, error)
}
