package urn

import (
	"crypto/rand"
	"errors"
	"math/big"
	"sync"
)

// UniqueRandomNumber struct contains a field "existing"
// which acts as a storage. For simplicity sync.Map is used.
// Depending on the needs, some interface type acting
// like a storage with relevant implementations could be used.
// Storage types such as B-tree, binary tree, Redis, Postgres
// could be used to guarantee uniqueness
type UniqueRandomNumber struct {
	maxInt     *big.Int
	existing   sync.Map
	maxRetries int
}

// New function returns unique random numbers generator. It requires expLimit
// which stands for the maximum exponent used in 2**MaxExp. maxRetries is number
// of times crypto/rand package will try to obtain the random number
func New(expLimit int, maxRetries int) *UniqueRandomNumber {
	return &UniqueRandomNumber{
		maxInt:     big.NewInt(0).Exp(big.NewInt(2), big.NewInt(200), nil),
		maxRetries: maxRetries,
	}
}

func (u *UniqueRandomNumber) GetUniqueRandomBigInt() (*big.Int, error) {
	n, err := u.getNumber()
	if err != nil {
		return nil, err
	}

	if _, ok := u.existing.Load(n.String()); !ok {
		u.existing.Store(n.String(), struct{}{})
		return n, nil
	}

	return u.GetUniqueRandomBigInt()
}

func (u *UniqueRandomNumber) getNumber() (*big.Int, error) {

	var (
		err   error
		n     *big.Int
		count int
	)

	err = errors.New("error")
	count = 0

	for err != nil && count < u.maxRetries {
		n, err = rand.Int(rand.Reader, u.maxInt)
		count += 1
	}

	return n, err
}
