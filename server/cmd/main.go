package main

import (
	"task/internal/server"
	"task/internal/services/urn"
)

func main() {

	// Randomly chosen expLimit and maxRetries
	expLimit := 200
	maxRetries := 20
	serverPort := 8080
	randomNumberService := urn.New(expLimit, maxRetries)

	srv := server.New(randomNumberService)

	srv.Run(serverPort)
}
